#!/bin/bash

healthy='#859900'
low='#b58900'
discharge='#dc322f'

capacity=`cat /sys/class/power_supply/BAT1/capacity`
if (($capacity <= 25));
then
    capacityColor=$low
else
    capacityColor=$healthy
fi

status=`cat /sys/class/power_supply/BAT1/status`

#if [[ "$status" = "Discharging" ]]
#then
#    statusColour=$discharge
#    status="▼"
#else
#    statusColour=$healthy
#    status="▲"
#fi

case "$status" in
    "Discharging")
        statusColor=$discharge
        status="▼ "
        ;;
    "Full")
        statusColor=$healthy
        status="ϟ "
        ;;
    "Charging")
        statusColor=$healthy
        status="▲ "
        ;;
    *)
        statusColor=$discharge
        status="??? "
esac
        

echo "<span>$capacity%</span> <span><b>$status</b></span>"
